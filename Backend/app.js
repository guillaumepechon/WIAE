var bodyParser = require('body-parser')
var http = require('http');
var express = require('express');
const WIAEROUTER = require('./routes/smartcontract.routes');
const app = express();
var morgan = require('morgan');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(morgan('dev'));
WIAEROUTER(app);
app.listen(3000, () => console.log('port 3000!!!'));
module.exports = app;
