pragma solidity ^0.4.18;
contract REACH {
    address airbus;
    struct substance {
        string Code_interne_de_substance;
        string nom_substance;
        string NumEC ;
        string NumCAS ;
        string Date_de_la_declaration;
        string Origine_substance_pays_couvert; 
    }
    uint[] Poids_de_la_substance;
    uint[] produit_substance;
    uint[] substance_produit;
    struct produit {
        string Code_fournisseur_DUNS;
        string Code_article_fournisseur ;
        string Poids_de_l_article ;
    }
    mapping (address=>uint[]) public produitMap;
    produit[] produitList;
    mapping (address=>uint[]) public substanceMap;
    substance[] substanceList;
  
    function REACH () public {
     airbus = msg.sender;
    }
    function ajouter_Substance(string Code_interne_de_substance,string nom_substance ,string NumEC,string NumCAS, string Date_de_la_declaration ,string Origine_substance_pays_couvert) public {
            substance memory nouvelleSubstance = substance(Code_interne_de_substance,nom_substance,NumEC,NumCAS,Date_de_la_declaration,Origine_substance_pays_couvert);
            substanceMap[msg.sender].push(substanceList.length);
            substanceList.push(nouvelleSubstance);

    }
    function getSubstanceInfo(uint id_substance) public view returns(string, string,string,string,string,string) {
        return (substanceList[id_substance].Code_interne_de_substance,substanceList[id_substance].nom_substance,substanceList[id_substance].NumEC,substanceList[id_substance].NumCAS,substanceList[id_substance].Date_de_la_declaration,substanceList[id_substance].Origine_substance_pays_couvert);
    }
       function getsubstanceList() public view returns(uint[]) {
        return substanceMap[airbus];
    }
    function ajouter_Produit(string Code_fournisseur_DUNS,string Code_article_fournisseur, string Poids_de_l_article ) public {
            produit memory nouvelleProduit = produit(Code_fournisseur_DUNS, Code_article_fournisseur, Poids_de_l_article);
            produitMap[msg.sender].push(produitList.length);
            produitList.push(nouvelleProduit);
    }
    function getProduitInfo(uint id_produit) public view returns(string, string,string) {
        return (produitList[id_produit].Code_fournisseur_DUNS,produitList[id_produit].Code_article_fournisseur,produitList[id_produit].Poids_de_l_article);
    }
    function getProduitList() public view returns(uint[]) {
        return substanceMap[msg.sender];
    }
    function ajouter_Substance_Au_Produit(uint poids, uint id_produitList,uint id_substanceList) public {
        Poids_de_la_substance.push(poids);
        produit_substance.push(id_produitList);
        substance_produit.push(id_substanceList);
    }
      function substance_Dans_Prouit() public view returns(uint[]) {
      return produit_substance ;
    }
    function substance_Dans_Pt() public view returns(uint[]) {
      return substance_produit ;
    }
    
    
    
    
}

