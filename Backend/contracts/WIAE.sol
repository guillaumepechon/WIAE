pragma solidity ^0.5.13;
pragma experimental ABIEncoderV2;

contract wiae {
    address acteur;
    
    constructor() public {
     acteur = msg.sender;
    }
    
    
    //eleveur
        //struct
        struct declaration_eleveur {
            uint256 Id_Eleveur;
            string Mode_Elevage;
            string Lieu_Elevage;
            string Id_Bovin;
            string Race;
            string Date_de_naissance;
            string Liste_Antibio;
            string Alimentation;
        }
    
        //list
        declaration_eleveur[] declaration_eleveurList;
        declaration_eleveur[] Mes_DeclaEleveurList;
        
        
        //function
        function newDeclaration_Eleveur(uint256 Id_Eleveur, string memory Mode_Elevage, string memory Lieu_Elevage, string memory Id_Bovin, string memory Race, string memory Date_de_naissance, string memory Liste_Antibio, string memory Alimentation) public {
            declaration_eleveur memory nouvelle_declaration_eleveur = declaration_eleveur(Id_Eleveur, Mode_Elevage, Lieu_Elevage, Id_Bovin, Race, Date_de_naissance, Liste_Antibio, Alimentation);
            declaration_eleveurList.push(nouvelle_declaration_eleveur);
        }
    
    
        function getDeclaration_EleveurById_Bovin(string memory Id_Bovin) public view returns(uint256, string memory, string memory, string memory, string memory, string memory, string memory, string memory) {
            uint id;
            for(uint i=0; i<declaration_eleveurList.length; i++) {
                if(keccak256(abi.encodePacked(declaration_eleveurList[i].Id_Bovin)) == keccak256(abi.encodePacked(Id_Bovin))) {
                    id = i;
                    break;
                }
            }
            return (declaration_eleveurList[id].Id_Eleveur, declaration_eleveurList[id].Mode_Elevage, declaration_eleveurList[id].Lieu_Elevage, declaration_eleveurList[id].Id_Bovin, declaration_eleveurList[id].Race, declaration_eleveurList[id].Date_de_naissance, declaration_eleveurList[id].Liste_Antibio, declaration_eleveurList[id].Alimentation);
        }
        
        
        function postList_Declaration_Eleveur(uint256 Id_Eleveur) public returns(declaration_eleveur[] memory) {
            Mes_DeclaEleveurList.length = 0;
            for(uint i=0; i<declaration_eleveurList.length; i++) {
                if(declaration_eleveurList[i].Id_Eleveur == Id_Eleveur) {
                    declaration_eleveur memory new_declaration_eleveurList = declaration_eleveur(declaration_eleveurList[i].Id_Eleveur,declaration_eleveurList[i].Mode_Elevage,declaration_eleveurList[i].Lieu_Elevage,declaration_eleveurList[i].Id_Bovin,declaration_eleveurList[i].Race,declaration_eleveurList[i].Date_de_naissance,declaration_eleveurList[i].Liste_Antibio,declaration_eleveurList[i].Alimentation);
                    Mes_DeclaEleveurList.push(new_declaration_eleveurList);
                }
            }
            return Mes_DeclaEleveurList;
        }
        
        function getList_Declaration_Eleveur() public view returns (declaration_eleveur[] memory) {
            return Mes_DeclaEleveurList;
        }
    
    
    
    //abatteur
        //Struct
        struct declaration_abatteur {
            uint256 Id_Abatteur;
            string Id_Bovin;
            string Date_Reception;
            string Date_Abattage;
            string Lieu_Abattage;
            string Saignee;
            string Date_Limite_Consommation;
        }
        
        //List
        declaration_abatteur[] declaration_abatteurList;
        declaration_abatteur[] Mes_DeclaAbatteurList;
        
        //Function
        function newDeclaration_Abatteur(uint256 Id_Abatteur, string memory Id_Bovin, string memory Date_Reception, string memory Date_Abattage, string memory Lieu_Abattage, string memory Saignee, string memory Date_Limite_Consommation) public {
                declaration_abatteur memory nouvelle_declaration_abatteur = declaration_abatteur(Id_Abatteur,Id_Bovin,Date_Reception,Date_Abattage,Lieu_Abattage,Saignee,Date_Limite_Consommation);
                declaration_abatteurList.push(nouvelle_declaration_abatteur);
        }
        
        function getDeclaration_AbatteurById_Bovin(string memory Id_Bovin) public view returns(uint256, string memory, string memory, string memory, string memory, string memory, string memory) {
            uint id;
            for(uint i=0; i<declaration_abatteurList.length; i++) {
                if(keccak256(abi.encodePacked(declaration_abatteurList[i].Id_Bovin)) == keccak256(abi.encodePacked(Id_Bovin))) {
                    id = i;
                    break;
                }
            }
            return (declaration_abatteurList[id].Id_Abatteur, declaration_abatteurList[id].Id_Bovin, declaration_abatteurList[id].Date_Reception, declaration_abatteurList[id].Date_Abattage, declaration_abatteurList[id].Lieu_Abattage,declaration_abatteurList[id].Saignee, declaration_abatteurList[id].Date_Limite_Consommation);
        }
        
        function postList_Declaration_Abatteur(uint256 Id_Abatteur) public returns(declaration_abatteur[] memory) {
            Mes_DeclaAbatteurList.length = 0;
            for(uint i=0; i<declaration_abatteurList.length; i++) {
                if(declaration_abatteurList[i].Id_Abatteur == Id_Abatteur) {
                    declaration_abatteur memory new_declaration_abatteurList = declaration_abatteur(declaration_abatteurList[i].Id_Abatteur,declaration_abatteurList[i].Id_Bovin,declaration_abatteurList[i].Date_Reception,declaration_abatteurList[i].Date_Abattage, declaration_abatteurList[i].Lieu_Abattage, declaration_abatteurList[i].Saignee,declaration_abatteurList[i].Date_Limite_Consommation);
                    Mes_DeclaAbatteurList.push(new_declaration_abatteurList);
                }
            }
            return Mes_DeclaAbatteurList;
        }
        
        function getList_Declaration_Abatteur() public view returns (declaration_abatteur[] memory) {
            return Mes_DeclaAbatteurList;
        }
        
    
    //transformateur
        //struct 
        struct declaration_transformateur {
            uint256 Id_Transformateur;
            string Id_Bovin;
            string Lieu_Transformation;
            string Morceau_Produit;
            string Date_Decoupe;
            uint256 Id_Morceau;
            string ITP;
        }
        
        //List
        mapping (string => uint) public Id_MorceauMap;
        declaration_transformateur[] declaration_transformateurList;
        declaration_transformateur[] Mes_DeclaTransformateurList;
        
        //Function
        function getMapIdMorceau(string memory Id_Bovin) public view returns(uint){
            return Id_MorceauMap[Id_Bovin];      
        }
        
        function newDeclaration_Transformateur(uint256 Id_Transformateur, string memory Id_Bovin, string memory Lieu_Transformation, string memory Morceau_Produit, string memory Date_Decoupe, string memory ITP) public {
            Id_MorceauMap[Id_Bovin]++;
            uint256 Id_Morceau = Id_MorceauMap[Id_Bovin];
            declaration_transformateur memory nouvelle_declaration_transformateur = declaration_transformateur(Id_Transformateur, Id_Bovin, Lieu_Transformation, Morceau_Produit, Date_Decoupe, Id_Morceau, ITP);
            declaration_transformateurList.push(nouvelle_declaration_transformateur);
        }
        
        function getDeclaration_Transformateur(string memory Id_Bovin, uint256 Id_Morceau) public view returns(uint256, string memory, string memory, string memory, string memory, uint256, string memory) {
            uint id = 0;
            for(uint i=0; i<declaration_transformateurList.length; i++) {
                if(keccak256(abi.encodePacked(declaration_transformateurList[i].Id_Bovin)) == keccak256(abi.encodePacked(Id_Bovin)) && declaration_transformateurList[i].Id_Morceau == Id_Morceau) {
                    id = i;
                    break;
                }
            }
            return (declaration_transformateurList[id].Id_Transformateur, declaration_transformateurList[id].Id_Bovin, declaration_transformateurList[id].Lieu_Transformation, declaration_transformateurList[id].Morceau_Produit, declaration_transformateurList[id].Date_Decoupe, declaration_transformateurList[id].Id_Morceau, declaration_transformateurList[id].ITP);
        }
        
        function postList_Declaration_Transformateur(uint256 Id_Transformateur) public returns(declaration_transformateur[] memory) {
            Mes_DeclaTransformateurList.length = 0;
            for(uint i=0; i<declaration_transformateurList.length; i++) {
                if(declaration_transformateurList[i].Id_Transformateur == Id_Transformateur) {
                    declaration_transformateur memory new_declaration_transformateurList = declaration_transformateur(declaration_transformateurList[i].Id_Transformateur, declaration_transformateurList[i].Id_Bovin, declaration_transformateurList[i].Lieu_Transformation, declaration_transformateurList[i].Morceau_Produit, declaration_transformateurList[i].Date_Decoupe, declaration_transformateurList[i].Id_Morceau, declaration_transformateurList[i].ITP);
                    Mes_DeclaTransformateurList.push(new_declaration_transformateurList);
                }
            }
            return Mes_DeclaTransformateurList;
        }
        
        function getList_Declaration_Transformateur() public view returns (declaration_transformateur[] memory) {
            return Mes_DeclaTransformateurList;
        }
        
        
    
    
    
    
    
}