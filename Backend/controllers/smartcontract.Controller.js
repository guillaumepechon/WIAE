var Web3 = require('web3');
web3 = new Web3(new Web3.providers.HttpProvider("HTTP://127.0.0.1:7555"));
web3.eth.defaultAccount = web3.eth.accounts[0];

var WIAESMARTCONTRACT = new web3.eth.Contract([
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"name": "Id_MorceauMap",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			}
		],
		"name": "getDeclaration_AbatteurById_Bovin",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			}
		],
		"name": "getDeclaration_EleveurById_Bovin",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			},
			{
				"internalType": "uint256",
				"name": "Id_Morceau",
				"type": "uint256"
			}
		],
		"name": "getDeclaration_Transformateur",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getList_Declaration_Abatteur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Abatteur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "Id_Bovin",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Reception",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Abattage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Abattage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Saignee",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Limite_Consommation",
						"type": "string"
					}
				],
				"internalType": "struct wiae.declaration_abatteur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getList_Declaration_Eleveur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Eleveur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "Mode_Elevage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Elevage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Id_Bovin",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Race",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_de_naissance",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Liste_Antibio",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Alimentation",
						"type": "string"
					}
				],
				"internalType": "struct wiae.declaration_eleveur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getList_Declaration_Transformateur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Transformateur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "Id_Bovin",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Transformation",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Morceau_Produit",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Decoupe",
						"type": "string"
					},
					{
						"internalType": "uint256",
						"name": "Id_Morceau",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "ITP",
						"type": "string"
					}
				],
				"internalType": "struct wiae.declaration_transformateur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			}
		],
		"name": "getMapIdMorceau",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Abatteur",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Reception",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Abattage",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Lieu_Abattage",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Saignee",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Limite_Consommation",
				"type": "string"
			}
		],
		"name": "newDeclaration_Abatteur",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Eleveur",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "Mode_Elevage",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Lieu_Elevage",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Race",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_de_naissance",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Liste_Antibio",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Alimentation",
				"type": "string"
			}
		],
		"name": "newDeclaration_Eleveur",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Transformateur",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "Id_Bovin",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Lieu_Transformation",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Morceau_Produit",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Decoupe",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "ITP",
				"type": "string"
			}
		],
		"name": "newDeclaration_Transformateur",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Abatteur",
				"type": "uint256"
			}
		],
		"name": "postList_Declaration_Abatteur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Abatteur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "Id_Bovin",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Reception",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Abattage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Abattage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Saignee",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Limite_Consommation",
						"type": "string"
					}
				],
				"internalType": "struct wiae.declaration_abatteur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Eleveur",
				"type": "uint256"
			}
		],
		"name": "postList_Declaration_Eleveur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Eleveur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "Mode_Elevage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Elevage",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Id_Bovin",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Race",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_de_naissance",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Liste_Antibio",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Alimentation",
						"type": "string"
					}
				],
				"internalType": "struct wiae.declaration_eleveur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Transformateur",
				"type": "uint256"
			}
		],
		"name": "postList_Declaration_Transformateur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Transformateur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "Id_Bovin",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Transformation",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Morceau_Produit",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Decoupe",
						"type": "string"
					},
					{
						"internalType": "uint256",
						"name": "Id_Morceau",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "ITP",
						"type": "string"
					}
				],
				"internalType": "struct wiae.declaration_transformateur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	}
],"0xA494B4132F9E0bbCC9d5837ba2d62C0FD6b0292d");



//eleveur
exports.newDeclaration_Eleveur = (req, res) => {
	WIAESMARTCONTRACT.methods.newDeclaration_Eleveur(req.body.Id_Eleveur, req.body.Mode_Elevage, req.body.Lieu_Elevage, req.body.Id_Bovin, req.body.Race, req.body.Date_de_naissance, req.body.Liste_Antibio, req.body.Alimentation)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getDeclaration_EleveurById_Bovin = (req, res) => {
	WIAESMARTCONTRACT.methods.getDeclaration_EleveurById_Bovin(req.body.Id_Bovin).call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

exports.postList_Declaration_Eleveur = (req, res) => {
	WIAESMARTCONTRACT.methods.postList_Declaration_Eleveur(req.body.Id_Eleveur)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getList_Declaration_Eleveur = (req, res) => {
	WIAESMARTCONTRACT.methods.getList_Declaration_Eleveur().call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

//abatteur
exports.newDeclaration_Abatteur = (req, res) => {
	WIAESMARTCONTRACT.methods.newDeclaration_Abatteur(req.body.Id_Abatteur, req.body.Id_Bovin, req.body.Date_Reception, req.body.Date_Abattage, req.body.Lieu_Abattage, req.body.Saignee, req.body.Date_Limite_Consommation)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getDeclaration_AbatteurById_Bovin = (req, res) => {
	WIAESMARTCONTRACT.methods.getDeclaration_AbatteurById_Bovin(req.body.Id_Bovin).call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

exports.postList_Declaration_Abatteur = (req, res) => {
	WIAESMARTCONTRACT.methods.postList_Declaration_Abatteur(req.body.Id_Abatteur)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getList_Declaration_Abatteur = (req, res) => {
	WIAESMARTCONTRACT.methods.getList_Declaration_Abatteur().call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};


//transformateur
exports.getMapIdMorceau = (req, res) => {
	WIAESMARTCONTRACT.methods.getMapIdMorceau(req.body.Id_Bovin).call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

exports.newDeclaration_Transformateur = (req, res) => {
	WIAESMARTCONTRACT.methods.newDeclaration_Transformateur(req.body.Id_Transformateur, req.body.Id_Bovin, req.body.Lieu_Transformation, req.body.Morceau_Produit, req.body.Date_Decoupe, req.body.ITP)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getDeclaration_Transformateur = (req, res) => {
	WIAESMARTCONTRACT.methods.getDeclaration_Transformateur(req.body.Id_Bovin, req.body.Id_Morceau).call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

exports.postList_Declaration_Transformateur = (req, res) => {
	WIAESMARTCONTRACT.methods.postList_Declaration_Transformateur(req.body.Id_Transformateur)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getList_Declaration_Transformateur = (req, res) => {
	WIAESMARTCONTRACT.methods.getList_Declaration_Transformateur().call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};


var WAIESMARTCONTRACT = new web3.eth.Contract([
	{
		"inputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "constructor"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "ITP",
				"type": "string"
			}
		],
		"name": "getDeclaration_Distributeur",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"internalType": "string",
				"name": "ITP",
				"type": "string"
			}
		],
		"name": "getDeclaration_Transporteur",
		"outputs": [
			{
				"internalType": "uint256",
				"name": "",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getList_Declaration_Distributeur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Distributeur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "ITP",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Nom_Produit",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Code_Barre",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Prix",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Mode_Conservation",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Mise_En_Rayon",
						"type": "string"
					}
				],
				"internalType": "struct waie.declaration_distributeur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "getList_Declaration_Transporteur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Transporteur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "ITP",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Mode_Transport",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Technique_Conservation",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Temperature",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Depart",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Depart",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Arrivee",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Arrivee",
						"type": "string"
					}
				],
				"internalType": "struct waie.declaration_transporteur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Distributeur",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "ITP",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Nom_Produit",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Code_Barre",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Prix",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Mode_Conservation",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Mise_En_Rayon",
				"type": "string"
			}
		],
		"name": "newDeclaration_Distributeur",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Transporteur",
				"type": "uint256"
			},
			{
				"internalType": "string",
				"name": "ITP",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Mode_Transport",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Technique_Conservation",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Temperature",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Lieu_Depart",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Depart",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Lieu_Arrivee",
				"type": "string"
			},
			{
				"internalType": "string",
				"name": "Date_Arrivee",
				"type": "string"
			}
		],
		"name": "newDeclaration_Transporteur",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Distributeur",
				"type": "uint256"
			}
		],
		"name": "postList_Declaration_Distributeur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Distributeur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "ITP",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Nom_Produit",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Code_Barre",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Prix",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Mode_Conservation",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Mise_En_Rayon",
						"type": "string"
					}
				],
				"internalType": "struct waie.declaration_distributeur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"internalType": "uint256",
				"name": "Id_Transporteur",
				"type": "uint256"
			}
		],
		"name": "postList_Declaration_Transporteur",
		"outputs": [
			{
				"components": [
					{
						"internalType": "uint256",
						"name": "Id_Transporteur",
						"type": "uint256"
					},
					{
						"internalType": "string",
						"name": "ITP",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Mode_Transport",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Technique_Conservation",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Temperature",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Depart",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Depart",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Lieu_Arrivee",
						"type": "string"
					},
					{
						"internalType": "string",
						"name": "Date_Arrivee",
						"type": "string"
					}
				],
				"internalType": "struct waie.declaration_transporteur[]",
				"name": "",
				"type": "tuple[]"
			}
		],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	}
],"0x62EA8B1094b80Ab9A2385045eB2a05ef1874D21B");


//transporteur
exports.newDeclaration_Transporteur = (req, res) => {
	WAIESMARTCONTRACT.methods.newDeclaration_Transporteur(req.body.Id_Transporteur, req.body.ITP, req.body.Mode_Transport, req.body.Technique_Conservation, req.body.Temperature, req.body.Lieu_Depart, req.body.Date_Depart, req.body.Lieu_Arrivee, req.body.Date_Arrivee)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getDeclaration_Transporteur = (req, res) => {
	WAIESMARTCONTRACT.methods.getDeclaration_Transporteur(req.body.ITP).call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

exports.postList_Declaration_Transporteur = (req, res) => {
	WAIESMARTCONTRACT.methods.postList_Declaration_Transporteur(req.body.Id_Transporteur)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getList_Declaration_Transporteur = (req, res) => {
	WAIESMARTCONTRACT.methods.getList_Declaration_Transporteur().call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

//distributeur
exports.newDeclaration_Distributeur = (req, res) => {
	WAIESMARTCONTRACT.methods.newDeclaration_Distributeur(req.body.Id_Distributeur, req.body.ITP, req.body.Nom_Produit, req.body.Code_Barre, req.body.Prix, req.body.Mode_Conservation, req.body.Date_Mise_En_Rayon)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getDeclaration_Distributeur = (req, res) => {
	WAIESMARTCONTRACT.methods.getDeclaration_Distributeur(req.body.ITP).call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};

exports.postList_Declaration_Distributeur = (req, res) => {
	WAIESMARTCONTRACT.methods.postList_Declaration_Distributeur(req.body.Id_Distributeur)
	.send({from: "0x13A97eCEdF5e6Ba163EE70Dce93691285fecc036",gas:3000000},function(error,result){
	})
	.then(result => {res.send(result)})
	.catch(e=>res.status(602).send({ error: "failed " }));
};

exports.getList_Declaration_Distributeur = (req, res) => {
	WAIESMARTCONTRACT.methods.getList_Declaration_Distributeur().call(function(error,result){
		if(result){
		res.send(result)
		}else {
		res.status(404).send({error: "Not Found"})
		}
	})
};