module.exports = function(app) {
  const ControllerMethods = require('../controllers/smartcontract.Controller');
  app.route('/newDeclaration_Eleveur')
  .post(ControllerMethods.newDeclaration_Eleveur);
  app.route('/getDeclaration_EleveurById_Bovin')
  .post(ControllerMethods.getDeclaration_EleveurById_Bovin);
  app.route('/postList_Declaration_Eleveur')
  .post(ControllerMethods.postList_Declaration_Eleveur);
  app.route('/getList_Declaration_Eleveur')
  .get(ControllerMethods.getList_Declaration_Eleveur);
  app.route('/newDeclaration_Abatteur')
  .post(ControllerMethods.newDeclaration_Abatteur);
  app.route('/getDeclaration_AbatteurById_Bovin')
  .post(ControllerMethods.getDeclaration_AbatteurById_Bovin);
  app.route('/postList_Declaration_Abatteur')
  .post(ControllerMethods.postList_Declaration_Abatteur);
  app.route('/getList_Declaration_Abatteur')
  .get(ControllerMethods.getList_Declaration_Abatteur);
  app.route('/getMapIdMorceau')
  .post(ControllerMethods.getMapIdMorceau);
  app.route('/newDeclaration_Transformateur')
  .post(ControllerMethods.newDeclaration_Transformateur);
  app.route('/getDeclaration_Transformateur')
  .post(ControllerMethods.getDeclaration_Transformateur);
  app.route('/postList_Declaration_Transformateur')
  .post(ControllerMethods.postList_Declaration_Transformateur);
  app.route('/getList_Declaration_Transformateur')
  .get(ControllerMethods.getList_Declaration_Transformateur);
  
  //transporteur
  app.route('/newDeclaration_Transporteur')
  .post(ControllerMethods.newDeclaration_Transporteur);
  app.route('/getDeclaration_Transporteur')
  .post(ControllerMethods.getDeclaration_Transporteur);
  app.route('/postList_Declaration_Transporteur')
  .post(ControllerMethods.postList_Declaration_Transporteur);
  app.route('/getList_Declaration_Transporteur')
  .get(ControllerMethods.getList_Declaration_Transporteur);

  //distributeur
  app.route('/newDeclaration_Distributeur')
  .post(ControllerMethods.newDeclaration_Distributeur);
  app.route('/getDeclaration_Distributeur')
  .post(ControllerMethods.getDeclaration_Distributeur);
  app.route('/postList_Declaration_Distributeur')
  .post(ControllerMethods.postList_Declaration_Distributeur);
  app.route('/getList_Declaration_Distributeur')
  .get(ControllerMethods.getList_Declaration_Distributeur);

};
