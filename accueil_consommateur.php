<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WAIE - Accueil Consommateur</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <span class="navbar-brand mb-0 h1 text-white">What Am I Eating ?</span>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->
    <h1 class="text-center text-white mt-5">Bienvenue sur votre espace consommateur</h1>

    <div class="col-4 card text-center p-3 mt-5" style="border-radius: 20px; margin-right: auto; margin-left: auto;">
        <label for="itp" class="font-weight-bold mt-2">Identifiant de Traçabilité du Produit (ITP)</label>
        <div class="form-inline mr-auto ml-auto mb-3">
            <input type="text" class="form-control" name="itp" id="itp" placeholder="Ex: FR7512345678001" minlength="15" maxlength="15">
            <button type="button" class="ml-4 btn btn-success" onclick="search('itp', 'ok', 'ko')">Rechercher</button>
        </div>
    </div>

    <div class="w-75 text-center p-4 mt-4" id="ok" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;">
        
    </div>

    <div class="alert alert-danger w-25 text-center p-4 mt-4" id="ko" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;" role="alert">
        <p>Le produit n'a pas été trouvé! Merci de vérifier le ITP</p>
    </div>

<script>
    function search(itp_input, ok, ko){
        var id = document.getElementById(itp_input).value;
        var split_id = id.match(/.{3,12}/g); 
        var id_bovin = split_id[0];
        var test_id_mc_split = parseInt(split_id[1], 10);
        var id_mc = split_id[1];
        var divok = document.getElementById(ok);
        divok.style.display = "none";
        var divko = document.getElementById(ko);
        divko.style.display = "none";

        if(id.length < 15 || isNaN(test_id_mc_split)){
            var div = document.getElementById(ko);
            div.style.display = "";
        }else{

            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost:3000/getDeclaration_Distributeur",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache"
                },
                "data": {
                    "ITP": id
                }
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
                if(id == response[1]){
                    var div = document.getElementById(ok);
                    div.style.display = "";

                    var div_distri = document.createElement('div');
                    div.appendChild(div_distri);
                        var div_distri1 = document.createElement('div');
                        div_distri1.setAttribute("class", "mt-5 text-center");
                        div_distri.appendChild(div_distri1);
                            var i_distri1 = document.createElement('i');
                            i_distri1.setAttribute("class", "fas fa-store text-white d-inline");
                            i_distri1.setAttribute("style", "font-size: 60px;");
                            div_distri1.appendChild(i_distri1);
                            var h_distri1 = document.createElement('h3');
                            h_distri1.setAttribute("class", "ml-5 text-white d-inline");
                            h_distri1.innerHTML = "DISTRIBUTION";
                            div_distri1.appendChild(h_distri1);
                        var div_distri2 = document.createElement('div');
                        div_distri2.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                        div_distri.appendChild(div_distri2);
                            var distri_div_nom = document.createElement('div');
                            distri_div_nom.setAttribute("class", "text-center col");
                            div_distri2.appendChild(distri_div_nom);
                                var i_distri_nom = document.createElement('i');
                                i_distri_nom.setAttribute("class", "fas fa-tag text-white d-inline");
                                i_distri_nom.setAttribute("style", "font-size: 30px;");
                                distri_div_nom.appendChild(i_distri_nom);
                                var p_distri_nom = document.createElement('p');
                                p_distri_nom.setAttribute("class", "ml-3 text-white d-inline");
                                p_distri_nom.innerHTML = response[2];
                                distri_div_nom.appendChild(p_distri_nom);
                            var distri_div_cb = document.createElement('div');
                            distri_div_cb.setAttribute("class", "text-center col");
                            div_distri2.appendChild(distri_div_cb);
                                var i_distri_cb = document.createElement('i');
                                i_distri_cb.setAttribute("class", "fas fa-barcode text-white d-inline");
                                i_distri_cb.setAttribute("style", "font-size: 30px;");
                                distri_div_cb.appendChild(i_distri_cb);
                                var p_distri_cb = document.createElement('p');
                                p_distri_cb.setAttribute("class", "ml-3 text-white d-inline");
                                p_distri_cb.innerHTML = response[3];
                                distri_div_cb.appendChild(p_distri_cb);
                            var distri_div_prix = document.createElement('div');
                            distri_div_prix.setAttribute("class", "text-center col");
                            div_distri2.appendChild(distri_div_prix);
                                var i_distri_prix = document.createElement('i');
                                i_distri_prix.setAttribute("class", "far fa-money-bill-alt text-white d-inline");
                                i_distri_prix.setAttribute("style", "font-size: 30px;");
                                distri_div_prix.appendChild(i_distri_prix);
                                var p_distri_prix = document.createElement('p');
                                p_distri_prix.setAttribute("class", "ml-3 text-white d-inline");
                                p_distri_prix.innerHTML = response[4] + " €";
                                distri_div_prix.appendChild(p_distri_prix);
                        var div_distri3 = document.createElement('div');
                        div_distri3.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                        div_distri.appendChild(div_distri3);
                            var distri_div_mc = document.createElement('div');
                            distri_div_mc.setAttribute("class", "text-center col");
                            div_distri3.appendChild(distri_div_mc);
                                var i_distri_mc = document.createElement('i');
                                i_distri_mc.setAttribute("class", "fas fa-thermometer-three-quarters text-white d-inline");
                                i_distri_mc.setAttribute("style", "font-size: 30px;");
                                distri_div_mc.appendChild(i_distri_mc);
                                var p_distri_mc = document.createElement('p');
                                p_distri_mc.setAttribute("class", "ml-3 text-white d-inline");
                                p_distri_mc.innerHTML = response[5];
                                distri_div_mc.appendChild(p_distri_mc);
                            var distri_div_date = document.createElement('div');
                            distri_div_date.setAttribute("class", "text-center col");
                            div_distri3.appendChild(distri_div_date);
                                var i_distri_date = document.createElement('i');
                                i_distri_date.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                i_distri_date.setAttribute("style", "font-size: 30px;");
                                distri_div_date.appendChild(i_distri_date);
                                var p_distri_date = document.createElement('p');
                                p_distri_date.setAttribute("class", "ml-3 text-white d-inline");
                                p_distri_date.innerHTML = "Mise en rayon le " + response[6];
                                distri_div_date.appendChild(p_distri_date);
                            

                    var hr_distri = document.createElement('hr');
                    hr_distri.setAttribute("color", "white");
                    hr_distri.setAttribute("style", "height: 0.7px; width: 1000px;");
                    hr_distri.setAttribute("class", "my-5");
                    div.appendChild(hr_distri);


                    var settings2 = {
                        "async": true,
                        "crossDomain": true,
                        "url": "http://localhost:3000/getDeclaration_Transporteur",
                        "method": "POST",
                        "headers": {
                            "Content-Type": "application/x-www-form-urlencoded",
                            "cache-control": "no-cache"
                        },
                        "data": {
                            "ITP": id
                        }
                    }

                    $.ajax(settings2).done(function (response2) {
                        console.log(response2);

                        var div_transp = document.createElement('div');
                        div.appendChild(div_transp);
                            var div_transp1 = document.createElement('div');
                            div_transp1.setAttribute("class", "mt-5 text-center");
                            div_transp.appendChild(div_transp1);
                                var i_transp1 = document.createElement('i');
                                i_transp1.setAttribute("class", "fas fa-truck text-white d-inline");
                                i_transp1.setAttribute("style", "font-size: 60px;");
                                div_transp1.appendChild(i_transp1);
                                var h_transp1 = document.createElement('h3');
                                h_transp1.setAttribute("class", "ml-5 text-white d-inline");
                                h_transp1.innerHTML = "TRANSPORT";
                                div_transp1.appendChild(h_transp1);
                            var div_transp2 = document.createElement('div');
                            div_transp2.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                            div_transp.appendChild(div_transp2);
                                var transp_div_moyen = document.createElement('div');
                                transp_div_moyen.setAttribute("class", "text-center col");
                                div_transp2.appendChild(transp_div_moyen);
                                    var i_transp_moyen = document.createElement('i');
                                    i_transp_moyen.setAttribute("class", "fas fa-route text-white d-inline");
                                    i_transp_moyen.setAttribute("style", "font-size: 30px;");
                                    transp_div_moyen.appendChild(i_transp_moyen);
                                    var p_transp_moyen = document.createElement('p');
                                    p_transp_moyen.setAttribute("class", "ml-3 text-white d-inline");
                                    p_transp_moyen.innerHTML = "Mode de transport " + response2[2];
                                    transp_div_moyen.appendChild(p_transp_moyen);
                                var transp_div_temp = document.createElement('div');
                                transp_div_temp.setAttribute("class", "text-center col");
                                div_transp2.appendChild(transp_div_temp);
                                    var i_transp_mc = document.createElement('i');
                                    i_transp_mc.setAttribute("class", "fas fa-thermometer-three-quarters text-white d-inline");
                                    i_transp_mc.setAttribute("style", "font-size: 30px;");
                                    transp_div_temp.appendChild(i_transp_mc);
                                    var p_transp_mc = document.createElement('p');
                                    p_transp_mc.setAttribute("class", "ml-3 text-white d-inline");
                                    p_transp_mc.innerHTML = response2[3] + " à " + response2[4] + " °C";
                                    transp_div_temp.appendChild(p_transp_mc);
                            var div_transp3 = document.createElement('div');
                            div_transp3.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                            div_transp.appendChild(div_transp3);
                                var transp_div_dep = document.createElement('div');
                                transp_div_dep.setAttribute("class", "text-center col");
                                div_transp3.appendChild(transp_div_dep);
                                    var div_transp_lieu_dep = document.createElement('div');
                                    div_transp_lieu_dep.setAttribute("class", "text-center mb-3");
                                    transp_div_dep.appendChild(div_transp_lieu_dep);
                                        var i_transp_lieu = document.createElement('i');
                                        i_transp_lieu.setAttribute("class", "fas fa-map-marker-alt text-white d-inline");
                                        i_transp_lieu.setAttribute("style", "font-size: 30px;");
                                        div_transp_lieu_dep.appendChild(i_transp_lieu);
                                        var p_transp_lieu = document.createElement('p');
                                        p_transp_lieu.setAttribute("class", "ml-3 text-white d-inline");
                                        p_transp_lieu.innerHTML = response2[5];
                                        div_transp_lieu_dep.appendChild(p_transp_lieu);
                                    var div_transp_date_dep = document.createElement('div');
                                    div_transp_date_dep.setAttribute("class", "text-center mb-3");
                                    transp_div_dep.appendChild(div_transp_date_dep);
                                        var i_transp_date = document.createElement('i');
                                        i_transp_date.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                        i_transp_date.setAttribute("style", "font-size: 30px;");
                                        div_transp_date_dep.appendChild(i_transp_date);
                                        var p_transp_date = document.createElement('p');
                                        p_transp_date.setAttribute("class", "ml-3 text-white d-inline");
                                        p_transp_date.innerHTML = response2[6];
                                        div_transp_date_dep.appendChild(p_transp_date);
                                
                                var transp_div_fleche = document.createElement('div');
                                transp_div_fleche.setAttribute("class", "text-center col-1");
                                transp_div_fleche.setAttribute("style", "display: flex; justify-content: center; align-items: center;");
                                div_transp3.appendChild(transp_div_fleche);
                                    var i_transp_fleche = document.createElement('i');
                                    i_transp_fleche.setAttribute("class", "fas fa-arrow-right text-white d-inline");
                                    i_transp_fleche.setAttribute("style", "font-size: 38px; margin-bottom");
                                    transp_div_fleche.appendChild(i_transp_fleche);

                                var transp_div_arr = document.createElement('div');
                                transp_div_arr.setAttribute("class", "text-center col");
                                div_transp3.appendChild(transp_div_arr);
                                    var div_transp_lieu_arr = document.createElement('div');
                                    div_transp_lieu_arr.setAttribute("class", "text-center mb-3");
                                    transp_div_arr.appendChild(div_transp_lieu_arr);
                                        var i_transp_lieu = document.createElement('i');
                                        i_transp_lieu.setAttribute("class", "fas fa-map-marker-alt text-white d-inline");
                                        i_transp_lieu.setAttribute("style", "font-size: 30px;");
                                        div_transp_lieu_arr.appendChild(i_transp_lieu);
                                        var p_transp_lieu = document.createElement('p');
                                        p_transp_lieu.setAttribute("class", "ml-3 text-white d-inline");
                                        p_transp_lieu.innerHTML = response2[7];
                                        div_transp_lieu_arr.appendChild(p_transp_lieu);
                                    var div_transp_date_arr = document.createElement('div');
                                    div_transp_date_arr.setAttribute("class", "text-center mb-3");
                                    transp_div_arr.appendChild(div_transp_date_arr);
                                        var i_transp_date = document.createElement('i');
                                        i_transp_date.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                        i_transp_date.setAttribute("style", "font-size: 30px;");
                                        div_transp_date_arr.appendChild(i_transp_date);
                                        var p_transp_date = document.createElement('p');
                                        p_transp_date.setAttribute("class", "ml-3 text-white d-inline");
                                        p_transp_date.innerHTML = response2[8];
                                        div_transp_date_arr.appendChild(p_transp_date);

                        var hr_transp = document.createElement('hr');
                        hr_transp.setAttribute("color", "white");
                        hr_transp.setAttribute("style", "height: 0.7px; width: 1000px;");
                        hr_transp.setAttribute("class", "my-5");
                        div.appendChild(hr_transp);

                        var settings3 = {
                            "async": true,
                            "crossDomain": true,
                            "url": "http://localhost:3000/getDeclaration_Transformateur",
                            "method": "POST",
                            "headers": {
                                "Content-Type": "application/x-www-form-urlencoded",
                                "cache-control": "no-cache"
                            },
                            "data": {
                                "Id_Bovin": id_bovin,
                                "Id_Morceau": id_mc
                            }
                        }

                        $.ajax(settings3).done(function (response3) { 
                            console.log(response3);


                        var div_transf = document.createElement('div');
                        div.appendChild(div_transf);
                            var div_transf1 = document.createElement('div');
                            div_transf1.setAttribute("class", "mt-5 text-center");
                            div_transf.appendChild(div_transf1);
                                var i_transf1 = document.createElement('i');
                                i_transf1.setAttribute("class", "fas fa-cut text-white d-inline");
                                i_transf1.setAttribute("style", "font-size: 60px;");
                                div_transf1.appendChild(i_transf1);
                                var h_transf1 = document.createElement('h3');
                                h_transf1.setAttribute("class", "ml-5 text-white d-inline");
                                h_transf1.innerHTML = "TRANSFORMATION";
                                div_transf1.appendChild(h_transf1);
                            var div_transf2 = document.createElement('div');
                            div_transf2.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                            div_transf.appendChild(div_transf2);
                                var transf_div_lieu = document.createElement('div');
                                transf_div_lieu.setAttribute("class", "text-center col");
                                div_transf2.appendChild(transf_div_lieu);
                                    var i_transf_lieu = document.createElement('i');
                                    i_transf_lieu.setAttribute("class", "fas fa-map-marker-alt text-white d-inline");
                                    i_transf_lieu.setAttribute("style", "font-size: 30px;");
                                    transf_div_lieu.appendChild(i_transf_lieu);
                                    var p_transf_lieu = document.createElement('p');
                                    p_transf_lieu.setAttribute("class", "ml-3 text-white d-inline");
                                    p_transf_lieu.innerHTML = response3[2];
                                    transf_div_lieu.appendChild(p_transf_lieu);
                            var div_transf3 = document.createElement('div');
                            div_transf3.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                            div_transf.appendChild(div_transf3);
                                var transf_div_mc = document.createElement('div');
                                transf_div_mc.setAttribute("class", "text-center col");
                                div_transf3.appendChild(transf_div_mc);
                                    var i_transf_mc = document.createElement('i');
                                    i_transf_mc.setAttribute("class", "fas fa-tag text-white d-inline");
                                    i_transf_mc.setAttribute("style", "font-size: 30px;");
                                    transf_div_mc.appendChild(i_transf_mc);
                                    var p_transf_mc = document.createElement('p');
                                    p_transf_mc.setAttribute("class", "ml-3 text-white d-inline");
                                    p_transf_mc.innerHTML = response3[3];
                                    transf_div_mc.appendChild(p_transf_mc);

                                var transf_div_date = document.createElement('div');
                                transf_div_date.setAttribute("class", "text-center col");
                                div_transf3.appendChild(transf_div_date);
                                    var i_transf_date = document.createElement('i');
                                    i_transf_date.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                    i_transf_date.setAttribute("style", "font-size: 30px;");
                                    transf_div_date.appendChild(i_transf_date);
                                    var p_transf_date = document.createElement('p');
                                    p_transf_date.setAttribute("class", "ml-3 text-white d-inline");
                                    p_transf_date.innerHTML = "Transformé le " + response3[4];
                                    transf_div_date.appendChild(p_transf_date);
                                

                        var hr_transf = document.createElement('hr');
                        hr_transf.setAttribute("color", "white");
                        hr_transf.setAttribute("style", "height: 0.7px; width: 1000px;");
                        hr_transf.setAttribute("class", "my-5");
                        div.appendChild(hr_transf);



                            
                            var settings4 = {
                                "async": true,
                                "crossDomain": true,
                                "url": "http://localhost:3000/getDeclaration_AbatteurById_Bovin",
                                "method": "POST",
                                "headers": {
                                    "Content-Type": "application/x-www-form-urlencoded",
                                    "cache-control": "no-cache"
                                },
                                "data": {
                                    "Id_Bovin": id_bovin
                                }
                            }

                            $.ajax(settings4).done(function (response4) {
                                console.log(response4);

                                var div_abatteur = document.createElement('div');
                                div.appendChild(div_abatteur);
                                    var div_abatteur1 = document.createElement('div');
                                    div_abatteur1.setAttribute("class", "mt-5 text-center");
                                    div_abatteur.appendChild(div_abatteur1);
                                        var i_abatteur1 = document.createElement('i');
                                        i_abatteur1.setAttribute("class", "fas fa-skull-crossbones text-white d-inline");
                                        i_abatteur1.setAttribute("style", "font-size: 60px;");
                                        div_abatteur1.appendChild(i_abatteur1);
                                        var h_abatteur1 = document.createElement('h3');
                                        h_abatteur1.setAttribute("class", "ml-5 text-white d-inline");
                                        h_abatteur1.innerHTML = "ABATTAGE";
                                        div_abatteur1.appendChild(h_abatteur1);
                                    var div_abatteur2 = document.createElement('div');
                                    div_abatteur2.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                                    div_abatteur.appendChild(div_abatteur2);
                                        var abatt_div_dt_rc = document.createElement('div');
                                        abatt_div_dt_rc.setAttribute("class", "text-center col");
                                        div_abatteur2.appendChild(abatt_div_dt_rc);
                                            var i_abatt_dt_rc = document.createElement('i');
                                            i_abatt_dt_rc.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                            i_abatt_dt_rc.setAttribute("style", "font-size: 30px;");
                                            abatt_div_dt_rc.appendChild(i_abatt_dt_rc);
                                            var p_abatt_dt_rc = document.createElement('p');
                                            p_abatt_dt_rc.setAttribute("class", "ml-3 text-white d-inline");
                                            p_abatt_dt_rc.innerHTML = "Reçu le " + response4[2];
                                            abatt_div_dt_rc.appendChild(p_abatt_dt_rc);
                                        var abatt_div_dt_ab = document.createElement('div');
                                        abatt_div_dt_ab.setAttribute("class", "text-center col");
                                        div_abatteur2.appendChild(abatt_div_dt_ab);
                                            var i_abatt_dt_ab = document.createElement('i');
                                            i_abatt_dt_ab.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                            i_abatt_dt_ab.setAttribute("style", "font-size: 30px;");
                                            abatt_div_dt_ab.appendChild(i_abatt_dt_ab);
                                            var p_abatt_dt_ab = document.createElement('p');
                                            p_abatt_dt_ab.setAttribute("class", "ml-3 text-white d-inline");
                                            p_abatt_dt_ab.innerHTML = "Abattu le " + response4[3];
                                            abatt_div_dt_ab.appendChild(p_abatt_dt_ab);                                    
                                    var div_abatteur3 = document.createElement('div');
                                    div_abatteur3.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                                    div_abatteur.appendChild(div_abatteur3);
                                        var abatt_div_lieu = document.createElement('div');
                                        abatt_div_lieu.setAttribute("class", "text-center col");
                                        div_abatteur3.appendChild(abatt_div_lieu);
                                            var i_abatt_lieu = document.createElement('i');
                                            i_abatt_lieu.setAttribute("class", "fas fa-map-marker-alt text-white d-inline");
                                            i_abatt_lieu.setAttribute("style", "font-size: 30px;");
                                            abatt_div_lieu.appendChild(i_abatt_lieu);
                                            var p_abatt_lieu = document.createElement('p');
                                            p_abatt_lieu.setAttribute("class", "ml-3 text-white d-inline");
                                            p_abatt_lieu.innerHTML = response4[4];
                                            abatt_div_lieu.appendChild(p_abatt_lieu);

                                    var div_abatteur4 = document.createElement('div');
                                    div_abatteur4.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                                    div_abatteur.appendChild(div_abatteur4);
                                        var abatt_div_md = document.createElement('div');
                                        abatt_div_md.setAttribute("class", "text-center col");
                                        div_abatteur4.appendChild(abatt_div_md);
                                            var i_abatt_md = document.createElement('i');
                                            i_abatt_md.setAttribute("class", "fas fa-pray text-white d-inline");
                                            i_abatt_md.setAttribute("style", "font-size: 30px;");
                                            abatt_div_md.appendChild(i_abatt_md);
                                            var p_abatt_md = document.createElement('p');
                                            p_abatt_md.setAttribute("class", "ml-3 text-white d-inline");
                                            p_abatt_md.innerHTML = response4[5];
                                            abatt_div_md.appendChild(p_abatt_md);
                                        var abatt_div_dt_lc = document.createElement('div');
                                        abatt_div_dt_lc.setAttribute("class", "text-center col");
                                        div_abatteur4.appendChild(abatt_div_dt_lc);
                                            var i_abatt_dt_lc = document.createElement('i');
                                            i_abatt_dt_lc.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                            i_abatt_dt_lc.setAttribute("style", "font-size: 30px;");
                                            abatt_div_dt_lc.appendChild(i_abatt_dt_lc);
                                            var p_abatt_dt_lc = document.createElement('p');
                                            p_abatt_dt_lc.setAttribute("class", "ml-3 text-white d-inline");
                                            p_abatt_dt_lc.innerHTML = "Date limite de consommation : " + response4[6];
                                            abatt_div_dt_lc.appendChild(p_abatt_dt_lc);  
                                        
                                var hr_abatteur = document.createElement('hr');
                                hr_abatteur.setAttribute("color", "white");
                                hr_abatteur.setAttribute("style", "height: 0.7px; width: 1000px;");
                                hr_abatteur.setAttribute("class", "my-5");
                                div.appendChild(hr_abatteur);


                                var settings5 = {
                                    "async": true,
                                    "crossDomain": true,
                                    "url": "http://localhost:3000/getDeclaration_EleveurById_Bovin",
                                    "method": "POST",
                                    "headers": {
                                        "Content-Type": "application/x-www-form-urlencoded",
                                        "cache-control": "no-cache"
                                    },
                                    "data": {
                                        "Id_Bovin": id_bovin
                                    }
                                }

                                $.ajax(settings5).done(function (response5) { 
                                    console.log(response5);
                                    var div_eleveur = document.createElement('div');
                                    div.appendChild(div_eleveur);
                                        var div_eleveur1 = document.createElement('div');
                                        div_eleveur1.setAttribute("class", "mt-5 text-center");
                                        div_eleveur.appendChild(div_eleveur1);
                                            var img_eleveur1 = document.createElement('img');
                                            img_eleveur1.setAttribute("class", "text-white d-inline");
                                            img_eleveur1.src = "./images/cow2.png";
                                            div_eleveur1.appendChild(img_eleveur1);
                                            var h_eleveur1 = document.createElement('h3');
                                            h_eleveur1.setAttribute("class", "ml-5 text-white d-inline");
                                            h_eleveur1.innerHTML = "ÉLEVAGE";
                                            div_eleveur1.appendChild(h_eleveur1);
                                        var div_eleveur2 = document.createElement('div');
                                        div_eleveur2.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                                        div_eleveur.appendChild(div_eleveur2);
                                            var elev_div_md = document.createElement('div');
                                            elev_div_md.setAttribute("class", "text-center col");
                                            div_eleveur2.appendChild(elev_div_md);
                                                var i_elev_md = document.createElement('img');
                                                i_elev_md.setAttribute("class", "text-white d-inline");
                                                i_elev_md.src = "./images/grass.png";
                                                elev_div_md.appendChild(i_elev_md);
                                                var p_elev_md = document.createElement('p');
                                                p_elev_md.setAttribute("class", "ml-3 text-white d-inline");
                                                p_elev_md.innerHTML = "Élevage " + response5[1];
                                                elev_div_md.appendChild(p_elev_md);
                                            var elev_div_race = document.createElement('div');
                                            elev_div_race.setAttribute("class", "text-center col");
                                            div_eleveur2.appendChild(elev_div_race);
                                                var i_elev_race = document.createElement('i');
                                                i_elev_race.setAttribute("class", "far fa-address-card text-white d-inline");
                                                i_elev_race.setAttribute("style", "font-size: 30px;");
                                                elev_div_race.appendChild(i_elev_race);
                                                var p_elev_race = document.createElement('p');
                                                p_elev_race.setAttribute("class", "ml-3 text-white d-inline");
                                                p_elev_race.innerHTML = "Race " + response5[4];
                                                elev_div_race.appendChild(p_elev_race);  
                                            var elev_div_dt = document.createElement('div');
                                            elev_div_dt.setAttribute("class", "text-center col");
                                            div_eleveur2.appendChild(elev_div_dt);
                                                var i_elev_dt = document.createElement('i');
                                                i_elev_dt.setAttribute("class", "far fa-calendar-alt text-white d-inline");
                                                i_elev_dt.setAttribute("style", "font-size: 30px;");
                                                elev_div_dt.appendChild(i_elev_dt);
                                                var p_elev_dt = document.createElement('p');
                                                p_elev_dt.setAttribute("class", "ml-3 text-white d-inline");
                                                p_elev_dt.innerHTML = "Né(e) le " + response5[5];
                                                elev_div_dt.appendChild(p_elev_dt);                                  
                                        var div_eleveur3 = document.createElement('div');
                                        div_eleveur3.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                                        div_eleveur.appendChild(div_eleveur3);
                                            var elev_div_lieu = document.createElement('div');
                                            elev_div_lieu.setAttribute("class", "text-center col");
                                            div_eleveur3.appendChild(elev_div_lieu);
                                                var i_elev_lieu = document.createElement('i');
                                                i_elev_lieu.setAttribute("class", "fas fa-map-marker-alt text-white d-inline");
                                                i_elev_lieu.setAttribute("style", "font-size: 30px;");
                                                elev_div_lieu.appendChild(i_elev_lieu);
                                                var p_elev_lieu = document.createElement('p');
                                                p_elev_lieu.setAttribute("class", "ml-3 text-white d-inline");
                                                p_elev_lieu.innerHTML = response5[2];
                                                elev_div_lieu.appendChild(p_elev_lieu);

                                        var div_eleveur4 = document.createElement('div');
                                        div_eleveur4.setAttribute("class", "text-center row mt-5 w-75 mx-auto");
                                        div_eleveur.appendChild(div_eleveur4);
                                            var elev_div_med = document.createElement('div');
                                            elev_div_med.setAttribute("class", "text-center col");
                                            div_eleveur4.appendChild(elev_div_med);
                                                var i_elev_med = document.createElement('i');
                                                i_elev_med.setAttribute("class", "fas fa-pills text-white d-inline");
                                                i_elev_med.setAttribute("style", "font-size: 30px;");
                                                elev_div_med.appendChild(i_elev_med);
                                                var p_elev_med = document.createElement('p');
                                                p_elev_med.setAttribute("class", "ml-3 text-white d-inline");
                                                p_elev_med.innerHTML = response5[6];
                                                elev_div_med.appendChild(p_elev_med);
                                            var elev_div_food = document.createElement('div');
                                            elev_div_food.setAttribute("class", "text-center col");
                                            div_eleveur4.appendChild(elev_div_food);
                                                var i_elev_food = document.createElement('i');
                                                i_elev_food.setAttribute("class", "fas fa-utensils text-white d-inline");
                                                i_elev_food.setAttribute("style", "font-size: 30px;");
                                                elev_div_food.appendChild(i_elev_food);
                                                var p_elev_food = document.createElement('p');
                                                p_elev_food.setAttribute("class", "ml-3 text-white d-inline");
                                                p_elev_food.innerHTML = response5[7];
                                                elev_div_food.appendChild(p_elev_food);  
                                            
                                });
                            });
                        });
                    });

                }else{
                    var div = document.getElementById(ko);
                    div.style.display = ""; 
                }
            });
        }
    }
        
</script>


</body>
</html>