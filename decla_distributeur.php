<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WAIE - Déclaration Distributeur</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
</head>
<body>
    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <a class="navbar-brand mb-0 h1 btn text-white" href="accueil_abatteur.php">What Am I Eating ?</a>
            <div>
                <a href="pre_decla_distributeur.php" class="btn mb-0 text-white">Déclarer une distribution</a>
                <a href="mes_decla_distributeur.php" class="btn mb-0 text-white" style="font-size: 17px;">Voir mes déclarations</a>
            </div>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->
    <h1 class="text-center text-white mt-5" style="margin-bottom: 70px;">Déclaration d'une nouvelle distribution</h1>
    <div class="container-fluid mb-5" style="width: 1300px; background-image: url('./images/distributeur.jpg'); background-size: 600px auto; background-repeat: no-repeat; background-size: cover; border-radius: 20px;">
        <div class="row">
            <div class="col-8">
            </div>
            <div class="col-4 card text-center" style="border-radius: 20px;">
                <div class="card-body">
                    <h3 class="card-title">Déclarer une distribution</h3>
                    <hr>
                    <h6 class="mb-5">ITP : <strong><?= $_GET['itp']?></strong><h6>
                    <form method="POST" action="decla_distributeurDB.php">
                        <input type="text" name="itp" id="itp" style="display: none;" value="<?= $_GET['itp']?>" readonly>
                        <div class="form-group mb-5">
                            <label for="nom_prod" style="font-size: 20px;">Nom du Produit</label>
                            <input type="text" class="form-control" name="nom_prod" id="nom_prod" placeholder="ex: Steack Haché Charal">
                        </div>
                        <div class="form-group mb-5">
                            <label for="code_barre" style="font-size: 20px;">Code Barre</label>
                            <input type="text" class="form-control" name="code_barre" id="code_barre" placeholder="ex: 3227061000023">
                        </div>
                        <div class="form-group mb-5">
                            <label for="prix" style="font-size: 20px;">Prix du produit (en €)</label>
                            <input type="text" class="form-control" name="prix" id="prix" placeholder="ex: 4,62">
                        </div>
                        <div class="form-group mb-5">
                            <label for="Technique" style="font-size: 20px;">Mode de conservation</label>
                            </br>
                            <select name="mode_cons" id="mode_cons" class="form-control">
                                <option value="" disabled selected>-- Sélectionner un mode de conservation --</option>
                                <option value="Réfrigération">Réfrigération</option>
                                <option value="Congélation">Congélation</option>
                                <option value="Surgélation">Surgélation</option>
                            </select>
                        </div>
                        <div class="form-group mb-5">
                            <label for="date_mr" style="font-size: 20px;">Date de mise en rayon</label>
                            <input type="date" class="form-control" name="date_mr" id="date_mr" value="<?php $dt_mr = date("Y-m-d"); echo $dt_mr;?>">
                        </div>
                        <hr>
                        <button type="submit" class="btn btn-success">Ajouter</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
