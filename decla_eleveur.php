<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WIAE - Accueil Eleveur</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
</head>
<body>
    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <a class="navbar-brand mb-0 h1 btn text-white" href="accueil_eleveur.php">What I Am Eating ?</a>
            <div>
                <a href="decla_eleveur.php" class="btn mb-0 text-white" style="font-weight: bold; font-size: 17px;">Déclarer un bovin</a>
                <a href="mes_decla_eleveur.php" class="btn mb-0 text-white" style="font-size: 17px;">Voir mes déclarations</a>
            </div>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->
    <h1 class="text-center text-white mt-5" style="margin-bottom: 70px;">Déclaration d'un nouveau bovin</h1>
    <div class="container-fluid mb-5" style="width: 1300px; background-image: url('./images/vache001.png'); background-size: 600px auto; background-repeat: no-repeat; background-size: cover; border-radius: 20px;">
        <div class="row">
            <div class="col-8">
            </div>
            <div class="col-4 card text-center" style="border-radius: 20px;">
                <div class="card-body">
                    <h3 class="card-title">Déclarer un bovin</h3>
                    <hr>
                    <form method="POST" action="decla_eleveurDB.php">
                        <div class="form-group mb-5">
                            <label for="mode_elevage" style="font-size: 20px;">Mode d'élevage</label>
                            </br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="mode_elevage" id="intensif" value="Intensif">
                                <label class="form-check-label" for="inlineRadio1">Intensif</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="mode_elevage" id="libre" value="Libre">
                                <label class="form-check-label" for="inlineRadio2">Libre</label>
                            </div>
                        </div>
                        <div class="form-group mb-5">
                            <label for="adresse" style="font-size: 20px;">Lieu d'élevage</label>
                            <div class="form-row mb-1">
                                <div class="col"><input type="text" name="nom_ferme" id="nom_ferme" class="form-control text-center" placeholder="Nom de la ferme"></div>
                            </div>
                            <div class="form-row mb-1">
                                <div class="col"><input type="text" name="rue" id="rue" class="form-control" placeholder="N° + Rue"></div>
                                <div class="col"><input type="text" name="cp" id="cp" class="form-control" placeholder="Code Postal"></div>
                            </div>
                            <div class="form-row">
                                <div class="col"><input type="text" name="ville" id="ville" class="form-control" placeholder="Ville"></div>
                                <div class="col">
                                    <select name="pays" class="form-control">
                                        <option value="France" selected="selected">France </option>

                                        <option value="Afghanistan">Afghanistan </option>
                                        <option value="Afrique Centrale">Afrique Centrale </option>
                                        <option value="Afrique du sud">Afrique du Sud </option>
                                        <option value="Albanie">Albanie </option>
                                        <option value="Algerie">Algerie </option>
                                        <option value="Allemagne">Allemagne </option>
                                        <option value="Andorre">Andorre </option>
                                        <option value="Angola">Angola </option>
                                        <option value="Anguilla">Anguilla </option>
                                        <option value="Arabie Saoudite">Arabie Saoudite </option>
                                        <option value="Argentine">Argentine </option>
                                        <option value="Armenie">Armenie </option>
                                        <option value="Australie">Australie </option>
                                        <option value="Autriche">Autriche </option>
                                        <option value="Azerbaidjan">Azerbaidjan </option>

                                        <option value="Bahamas">Bahamas </option>
                                        <option value="Bangladesh">Bangladesh </option>
                                        <option value="Barbade">Barbade </option>
                                        <option value="Bahrein">Bahrein </option>
                                        <option value="Belgique">Belgique </option>
                                        <option value="Belize">Belize </option>
                                        <option value="Benin">Benin </option>
                                        <option value="Bermudes">Bermudes </option>
                                        <option value="Bielorussie">Bielorussie </option>
                                        <option value="Bolivie">Bolivie </option>
                                        <option value="Botswana">Botswana </option>
                                        <option value="Bhoutan">Bhoutan </option>
                                        <option value="Boznie Herzegovine">Boznie Herzegovine </option>
                                        <option value="Bresil">Bresil </option>
                                        <option value="Brunei">Brunei </option>
                                        <option value="Bulgarie">Bulgarie </option>
                                        <option value="Burkina Faso">Burkina Faso </option>
                                        <option value="Burundi">Burundi </option>

                                        <option value="Caiman">Caiman </option>
                                        <option value="Cambodge">Cambodge </option>
                                        <option value="Cameroun">Cameroun </option>
                                        <option value="Canada">Canada </option>
                                        <option value="Canaries">Canaries </option>
                                        <option value="Cap Vert">Cap Vert </option>
                                        <option value="Chili">Chili </option>
                                        <option value="Chine">Chine </option>
                                        <option value="Chypre">Chypre </option>
                                        <option value="Colombie">Colombie </option>
                                        <option value="Comores">Colombie </option>
                                        <option value="Congo">Congo </option>
                                        <option value="Congo democratique">Congo democratique </option>
                                        <option value="Cook">Cook </option>
                                        <option value="Coree_du_Nord">Coree_du_Nord </option>
                                        <option value="Coree_du_Sud">Coree_du_Sud </option>
                                        <option value="Costa_Rica">Costa_Rica </option>
                                        <option value="Cote_d_Ivoire">Côte_d_Ivoire </option>
                                        <option value="Croatie">Croatie </option>
                                        <option value="Cuba">Cuba </option>

                                        <option value="Danemark">Danemark </option>
                                        <option value="Djibouti">Djibouti </option>
                                        <option value="Dominique">Dominique </option>

                                        <option value="Egypte">Egypte </option>
                                        <option value="Emirats Arabes Unis">Emirats Arabes Unis </option>
                                        <option value="Equateur">Equateur </option>
                                        <option value="Erythree">Erythree </option>
                                        <option value="Espagne">Espagne </option>
                                        <option value="Estonie">Estonie </option>
                                        <option value="Etats Unis">Etats Unis </option>
                                        <option value="Ethiopie">Ethiopie </option>

                                        <option value="Falkland">Falkland </option>
                                        <option value="Feroe">Feroe </option>
                                        <option value="Fidji">Fidji </option>
                                        <option value="Finlande">Finlande </option>
                                        <option value="France">France </option>

                                        <option value="Gabon">Gabon </option>
                                        <option value="Gambie">Gambie </option>
                                        <option value="Georgie">Georgie </option>
                                        <option value="Ghana">Ghana </option>
                                        <option value="Gibraltar">Gibraltar </option>
                                        <option value="Grece">Grece </option>
                                        <option value="Grenade">Grenade </option>
                                        <option value="Groenland">Groenland </option>
                                        <option value="Guadeloupe">Guadeloupe </option>
                                        <option value="Guam">Guam </option>
                                        <option value="Guatemala">Guatemala</option>
                                        <option value="Guernesey">Guernesey </option>
                                        <option value="Guinee">Guinee </option>
                                        <option value="Guinee Bissau">Guinee Bissau </option>
                                        <option value="Guinee equatoriale">Guinee Equatoriale </option>
                                        <option value="Guyana">Guyana </option>
                                        <option value="Guyane Francaise ">Guyane Francaise </option>

                                        <option value="Haiti">Haiti </option>
                                        <option value="Hawaii">Hawaii </option>
                                        <option value="Honduras">Honduras </option>
                                        <option value="Hong Kong">Hong Kong </option>
                                        <option value="Hongrie">Hongrie </option>

                                        <option value="Inde">Inde </option>
                                        <option value="Indonesie">Indonesie </option>
                                        <option value="Iran">Iran </option>
                                        <option value="Iraq">Iraq </option>
                                        <option value="Irlande">Irlande </option>
                                        <option value="Islande">Islande </option>
                                        <option value="Israel">Israel </option>
                                        <option value="Italie">italie </option>

                                        <option value="Jamaique">Jamaique </option>
                                        <option value="Jan Mayen">Jan Mayen </option>
                                        <option value="Japon">Japon </option>
                                        <option value="Jersey">Jersey </option>
                                        <option value="Jordanie">Jordanie </option>

                                        <option value="Kazakhstan">Kazakhstan </option>
                                        <option value="Kenya">Kenya </option>
                                        <option value="Kirghizstan">Kirghizistan </option>
                                        <option value="Kiribati">Kiribati </option>
                                        <option value="Koweit">Koweit </option>

                                        <option value="Laos">Laos </option>
                                        <option value="Lesotho">Lesotho </option>
                                        <option value="Lettonie">Lettonie </option>
                                        <option value="Liban">Liban </option>
                                        <option value="Liberia">Liberia </option>
                                        <option value="Liechtenstein">Liechtenstein </option>
                                        <option value="Lituanie">Lituanie </option>
                                        <option value="Luxembourg">Luxembourg </option>
                                        <option value="Lybie">Lybie </option>

                                        <option value="Macao">Macao </option>
                                        <option value="Macedoine">Macedoine </option>
                                        <option value="Madagascar">Madagascar </option>
                                        <option value="Madère">Madère </option>
                                        <option value="Malaisie">Malaisie </option>
                                        <option value="Malawi">Malawi </option>
                                        <option value="Maldives">Maldives </option>
                                        <option value="Mali">Mali </option>
                                        <option value="Malte">Malte </option>
                                        <option value="Man">Man </option>
                                        <option value="Mariannes du Nord">Mariannes du Nord </option>
                                        <option value="Maroc">Maroc </option>
                                        <option value="Marshall">Marshall </option>
                                        <option value="Martinique">Martinique </option>
                                        <option value="Maurice">Maurice </option>
                                        <option value="Mauritanie">Mauritanie </option>
                                        <option value="Mayotte">Mayotte </option>
                                        <option value="Mexique">Mexique </option>
                                        <option value="Micronesie">Micronesie </option>
                                        <option value="Midway">Midway </option>
                                        <option value="Moldavie">Moldavie </option>
                                        <option value="Monaco">Monaco </option>
                                        <option value="Mongolie">Mongolie </option>
                                        <option value="Montserrat">Montserrat </option>
                                        <option value="Mozambique">Mozambique </option>

                                        <option value="Namibie">Namibie </option>
                                        <option value="Nauru">Nauru </option>
                                        <option value="Nepal">Nepal </option>
                                        <option value="Nicaragua">Nicaragua </option>
                                        <option value="Niger">Niger </option>
                                        <option value="Nigeria">Nigeria </option>
                                        <option value="Niue">Niue </option>
                                        <option value="Norfolk">Norfolk </option>
                                        <option value="Norvege">Norvege </option>
                                        <option value="Nouvelle Caledonie">Nouvelle Caledonie </option>
                                        <option value="Nouvelle Zelande">Nouvelle Zelande </option>

                                        <option value="Oman">Oman </option>
                                        <option value="Ouganda">Ouganda </option>
                                        <option value="Ouzbekistan">Ouzbekistan </option>

                                        <option value="Pakistan">Pakistan </option>
                                        <option value="Palau">Palau </option>
                                        <option value="Palestine">Palestine </option>
                                        <option value="Panama">Panama </option>
                                        <option value="Papouasie Nouvelle Guinee">Papouasie Nouvelle Guinee </option>
                                        <option value="Paraguay">Paraguay </option>
                                        <option value="Pays Bas">Pays Bas </option>
                                        <option value="Perou">Perou </option>
                                        <option value="Philippines">Philippines </option>
                                        <option value="Pologne">Pologne </option>
                                        <option value="Polynesie">Polynesie </option>
                                        <option value="Porto Rico">Porto Rico </option>
                                        <option value="Portugal">Portugal </option>

                                        <option value="Qatar">Qatar </option>

                                        <option value="Republique Dominicaine">Republique Dominicaine </option>
                                        <option value="Republique Tchèque">Republique Tcheque </option>
                                        <option value="Reunion">Reunion </option>
                                        <option value="Roumanie">Roumanie </option>
                                        <option value="Royaume_Uni">Royaume_Uni </option>
                                        <option value="Russie">Russie </option>
                                        <option value="Rwanda">Rwanda </option>

                                        <option value="Sahara Occidental">Sahara Occidental </option>
                                        <option value="Sainte Lucie">Sainte Lucie </option>
                                        <option value="Saint_Marin">Saint_Marin </option>
                                        <option value="Salomon">Salomon </option>
                                        <option value="Salvador">Salvador </option>
                                        <option value="Samoa_Occidentales">Samoa_Occidentales</option>
                                        <option value="Samoa_Americaine">Samoa_Americaine </option>
                                        <option value="Sao_Tome_et_Principe">Sao_Tome_et_Principe </option>
                                        <option value="Senegal">Senegal </option>
                                        <option value="Seychelles">Seychelles </option>
                                        <option value="Sierra Leone">Sierra Leone </option>
                                        <option value="Singapour">Singapour </option>
                                        <option value="Slovaquie">Slovaquie </option>
                                        <option value="Slovenie">Slovenie</option>
                                        <option value="Somalie">Somalie </option>
                                        <option value="Soudan">Soudan </option>
                                        <option value="Sri_Lanka">Sri_Lanka </option>
                                        <option value="Suede">Suede </option>
                                        <option value="Suisse">Suisse </option>
                                        <option value="Surinam">Surinam </option>
                                        <option value="Swaziland">Swaziland </option>
                                        <option value="Syrie">Syrie </option>

                                        <option value="Tadjikistan">Tadjikistan </option>
                                        <option value="Taiwan">Taiwan </option>
                                        <option value="Tonga">Tonga </option>
                                        <option value="Tanzanie">Tanzanie </option>
                                        <option value="Tchad">Tchad </option>
                                        <option value="Thailande">Thailande </option>
                                        <option value="Tibet">Tibet </option>
                                        <option value="Timor_Oriental">Timor_Oriental </option>
                                        <option value="Togo">Togo </option>
                                        <option value="Trinite_et_Tobago">Trinite_et_Tobago </option>
                                        <option value="Tristan da cunha">Tristan de cuncha </option>
                                        <option value="Tunisie">Tunisie </option>
                                        <option value="Turkmenistan">Turmenistan </option>
                                        <option value="Turquie">Turquie </option>

                                        <option value="Ukraine">Ukraine </option>
                                        <option value="Uruguay">Uruguay </option>

                                        <option value="Vanuatu">Vanuatu </option>
                                        <option value="Vatican">Vatican </option>
                                        <option value="Venezuela">Venezuela </option>
                                        <option value="Vierges_Americaines">Vierges_Americaines </option>
                                        <option value="Vierges_Britanniques">Vierges_Britanniques </option>
                                        <option value="Vietnam">Vietnam </option>

                                        <option value="Wake">Wake </option>
                                        <option value="Wallis et Futuma">Wallis et Futuma </option>

                                        <option value="Yemen">Yemen </option>
                                        <option value="Yougoslavie">Yougoslavie </option>

                                        <option value="Zambie">Zambie </option>
                                        <option value="Zimbabwe">Zimbabwe </option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mb-5">
                            <label for="id_vache" style="font-size: 20px;">Identifiant du Bovin</label>
                            <input type="text" class="form-control" name="id_vache" id="id_vache" placeholder="Ex: FR7612345678" onchange="Is_Declared('id_vache', 'small_id_vache')">
                            <small class="form-text" id="small_id_vache" style="color: red; display: none;">Ce bovin est déjà déclaré!</small>
                        </div>
                        <div class="form-group mb-5">
                            <label for="race" style="font-size: 20px;">Race</label>
                            <input type="text" class="form-control" name="race" id="race" placeholder="Ex: Normande, Charolaise, Blonde d'Aquiaine...">
                        </div>
                        <div class="form-group mb-5">
                            <label for="date_naissance" style="font-size: 20px;">Date de naissance</label>
                            <input type="date" class="form-control" name="date_naissance" id="date_naissance" value="<?php $dob = date("Y-m-d"); echo $dob;?>">
                        </div>
                        <div class="form-group mb-5">
                            <label for="antibio_ck" style="font-size: 20px;">Traitement(s) antibiotique(s) reçu(s)</label>
                            </br>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="antibio_ck" id="oui" value="oui" onclick="affichage_check('oui', 'antibio')">
                                <label class="form-check-label" for="inlineRadio1">Oui</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="antibio_ck" id="non" value="non" onclick="masquage_check('non', 'antibio')">
                                <label class="form-check-label" for="inlineRadio2">Non</label>
                            </div>
                            <input type="text" class="form-control" name="antibio" id="antibio" placeholder="Liste des antibiotiques" style="display: none;">
                        </div>
                        <div class="form-group mb-5">
                            <label for="alimentation" style="font-size: 20px;">Alimentation reçu</label>
                            <input type="text" class="form-control" name="alimentation" id="alimentation" placeholder="Ex: Maïs fourrager, blé, orge, colza...">
                        </div>
                        <hr>
                        <button type="submit" name="declaration" id="btn_decla" class="btn btn-success">Déclarer</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function Is_Declared(input_id, small_id) {
            var input = document.getElementById(input_id);
            var input_val = input.value;
            var small = document.getElementById(small_id);
            
            
            var settings = {
                "async": true,
                "url": "http://localhost:3000/getDeclaration_EleveurById_Bovin",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache",
                    "Postman-Token": "6ece834f-fb7b-42db-849e-4a7e80bc6a8b"
                },
                "data": {
                    "Id_Bovin": input_val
                }
            }

            $.ajax(settings).done(function (response) {
                if(response[3] == input_val){
                    small.style.display = "";
                    document.getElementById("btn_decla").disabled = true;
                }else{
                    small.style.display = "none";
                    document.getElementById("btn_decla").disabled = false;
                }
                
            });

        }

        function affichage_check(check_id, input_id) {
            var checkBox = document.getElementById(check_id);
            var text = document.getElementById(input_id);
            if (checkBox.checked == true){
                text.style.display = "";
            } else {
                text.style.display = "none";
            }
        } 

        function masquage_check(check_id, input_id) {
            var checkBox = document.getElementById(check_id);
            var text = document.getElementById(input_id);
            if (checkBox.checked == true){
            text.style.display = "none";
            } else {
                text.style.display = "";
            }
        }

    </script>
</body>
</html>