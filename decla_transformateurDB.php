<?php 
session_start();
include_once('includes.php');
if(!empty($_POST)){
    extract($_POST);
} 
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WAIE - Accueil Transformateur</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
    

    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <a class="navbar-brand mb-0 h1 btn text-white" href="accueil_transformateur.php">What Am I Eating ?</a>
            <div>
                <a href="decla_transformateur.php" class="btn mb-0 text-white" style="font-weight: bold; font-size: 17px;">Déclarer une transformation</a>
                <a href="mes_decla_transformateur.php" class="btn mb-0 text-white" style="font-size: 17px;">Voir mes déclarations</a>
            </div>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->

        <div class="alert alert-success w-75 text-center"  style="margin-left: auto; margin-right: auto; border-radius: 10px;" role="alert">Votre déclaration concernant la transformation du bovin a bien été effectué</div>
        <a href="accueil_transformateur.php"  style="margin-left: auto; margin-right: auto;" class="btn btn-warning">Retour à ma page d'accueil</a>
        <script>
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost:3000/newDeclaration_Transformateur",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache"
                },
                "data": {
                    "Id_Transformateur": "<?= $_SESSION['id_utilisateur'] ?>",
                    "Id_Bovin": "<?= $id_bovin ?>",
                    "Lieu_Transformation": "<?php $lieu = $nom_trans . ", " . $rue . " " . $cp . " " . $ville . ", " . $pays; echo $lieu;?>",
                    "Morceau_Produit": "<?= $morc ?>",
                    "Date_Decoupe": "<?php $dt_trans= date("d/m/Y", strtotime($date_cut)); echo $dt_trans;?>",
                    "ITP": "<?= $itp ?>"
                }
            }

            $.ajax(settings).done(function (response) {
                console.log(response);
            });
        </script>
       
</body>
</html>

   
