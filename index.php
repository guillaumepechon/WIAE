<?php session_start();?>
<!DOCTYPE html>
<html lang="fr" >
<head>
  <meta charset="UTF-8">
  <title>What Am I Eating ?</title>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
  <link rel="stylesheet" href="./style.css">
</head>
<body>
	<!-- début message d'erreur -->
		<?php if(isset($_SESSION['tmp_err_prenom_r_vide']) && $_SESSION['tmp_err_prenom_r_vide'] == 1 && isset($_SESSION['err_prenom_r_vide'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_prenom_r_vide']?></div>
		<?php $_SESSION['tmp_err_prenom_r_vide'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_nom_r_vide']) && $_SESSION['tmp_err_nom_r_vide'] == 1 && isset($_SESSION['err_nom_r_vide'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_nom_r_vide']?></div>
		<?php $_SESSION['tmp_err_nom_r_vide'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_email_r_vide']) && $_SESSION['tmp_err_email_r_vide'] == 1 && isset($_SESSION['err_email_r_vide'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_email_r_vide']?></div>
		<?php $_SESSION['tmp_err_email_r_vide'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_mdp_r_vide']) && $_SESSION['tmp_err_mdp_r_vide'] == 1 && isset($_SESSION['err_mdp_r_vide'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_mdp_r_vide']?></div>
		<?php $_SESSION['tmp_err_mdp_r_vide'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_conf_mdp_r_vide']) && $_SESSION['tmp_err_conf_mdp_r_vide'] == 1 && isset($_SESSION['err_conf_mdp_r_vide'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_conf_mdp_r_vide']?></div>
		<?php $_SESSION['tmp_err_conf_mdp_r_vide'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_statut_r_vide']) && $_SESSION['tmp_err_statut_r_vide'] == 1 && isset($_SESSION['err_statut_r_vide'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_statut_r_vide']?></div>
		<?php $_SESSION['tmp_err_statut_r_vide'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_mdp_r']) && $_SESSION['tmp_err_mdp_r'] == 1 && isset($_SESSION['err_mdp_r'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_mdp_r']?></div>
		<?php $_SESSION['tmp_err_mdp_r'] = 0; }?>

		<?php if(isset($_SESSION['tmp_err_email_r_false']) && $_SESSION['tmp_err_email_r_false'] == 1 && isset($_SESSION['err_email_r_false'])) { ?>
			<div class="alert alert-danger" role="alert"><?= $_SESSION['err_email_r_false']?></div>
		<?php $_SESSION['tmp_err_email_r_false'] = 0; }?>

		<?php if(isset($_SESSION['tmp_register_ok']) && $_SESSION['tmp_register_ok'] == 1 && isset($_SESSION['register_ok'])) { ?>
			<div class="alert alert-success" role="alert"><?= $_SESSION['register_ok']?></div>
		<?php $_SESSION['tmp_register_ok'] = 0; }?>
	<!-- fin message d'erreur -->
<!-- partial:index.partial.html -->
<h1 style="color: white;">Bienvenue sur What am I eating ?</h1>
<div class="container" id="container">
	<div class="form-container sign-up-container">
		<form method="POST" action="register.php">
			<h2>Créer un Compte</h2>
			<!--<div class="social-container">
				<a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
				<a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
				<a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
			</div>-->
			<select style="background-color: #eee; border: none; padding: 12px 15px; margin: 8px 0 16px 0; width: 100%;" name="statut_r" id="statut_r">
				<option value="" disabled selected>-- Sélectionner mon statut --</option>
				<option value="eleveur">Eleveur</option>
				<option value="abatteur">Abatteur</option>
				<option value="transformateur">Transformateur</option>
				<option value="transporteur">Transporteur</option>
				<option value="distributeur">Distributeur</option>
				<option value="consommateur">Consommateur</option>
			</select>
			<input type="text" name="prenom_r" id="prenom_r" placeholder="Prenom"/>
			<input type="text" name="nom_r" id="nom_r" placeholder="Nom"/>
			<input type="email" name="email_r" id="email_r" placeholder="Email" />
			<input type="password" name="mdp_r" id="mdp_r" placeholder="Mot de Passe" />
			<input type="password" name="conf_mdp_r" id="conf_mdp_r" placeholder="Confirmer le mot de passe" />
			<button type="submit" name="inscription">inscription</button>
		</form>
	</div>
	<div class="form-container sign-in-container">
		<form method="POST" action="login.php">
			<h2>Se Connecter</h2>
			<!--<div class="social-container">
				<a href="#" class="social"><i class="fab fa-facebook-f"></i></a>
				<a href="#" class="social"><i class="fab fa-google-plus-g"></i></a>
				<a href="#" class="social"><i class="fab fa-linkedin-in"></i></a>
			</div>-->
			<input type="email" name="email_li" id="email_li" placeholder="Email" />
			<input type="password" name="mdp_li" id="mdp_li" placeholder="Mot de Passe" />
			<a href="#">Mot de passe oublié?</a>
			<button type="submit" name="connexion">connexion</button>
		</form>
	</div>
	<div class="overlay-container">
		<div class="overlay">
			<div class="overlay-panel overlay-left">
				<h1>ReBonjour !</h1>
				<p>Tu possèdes déjà un compte ? </br> Connecte-toi vite !</p>
				<button class="ghost" id="signIn">Connexion</button>
			</div>
			<div class="overlay-panel overlay-right">
				<h1>Bonjour !</h1>
				<p>Tu as envie d'en savoir plus sur ce que tu as dans ton assieste ? Rejoinds-nous!</p>
				<button class="ghost" id="signUp">Inscription</button>
			</div>
		</div>
	</div>
</div>

<!-- <footer>
	<p>
		Created with <i class="fa fa-heart"></i> by
		<a target="_blank" href="https://florin-pop.com">Florin Pop</a>
		- Read how I created this and how you can join the challenge
		<a target="_blank" href="https://www.florin-pop.com/blog/2019/03/double-slider-sign-in-up-form/">here</a>.
	</p>
</footer>-->
<!-- partial -->
  <script  src="./script.js"></script>

</body>
</html>