<?php 
session_start();
include_once('includes.php');
if(!empty($_POST)){
    extract($_POST);
    $valid = true;
    if(isset($_POST['connexion']))
    {
        $email_li = htmlentities(strtolower(trim($email_li)));
        $mdp_li = trim($mdp_li);

        if(empty($email_li)){ // Vérification qu'il y est bien un mail de renseigné
            $valid = false;
            $_SESSION['err_mail_li_vide'] = "Il faut rentrer votre email";
            $_SESSION['tmp_err_mail_li_vide'] = 1;
        }

        if(empty($mdp_li)){ // Vérification qu'il y est bien un mot de passe de renseigné
            $valid = false;
            $_SESSION['err_mdp_li_vide'] = "Il faut rentrer votre mot de passe";
            $_SESSION['tmp_err_mdp_li_vide'] = 1;
        }

        $req = $DB->query("SELECT * FROM acteur WHERE Act_Email = ? AND Act_Mdp = ?", array($email_li, $mdp_li));
        $req = $req->fetch();
        
        if ($req == null){
            $valid = false;
            $_SESSION['err_id_false'] = "L'email ou le mot de passe est incorrecte";
            $_SESSION['tmp_err_id_false'] = 1;
        }
        
        if ($valid){

            $_SESSION['nom'] = $req['Act_Nom'];
            $_SESSION['prenom'] = $req['Act_Prenom'];
            $_SESSION['email'] = $req['Act_Email'];
            $_SESSION['statut'] = $req['Act_Statut'];
            $_SESSION['id_utilisateur'] = $req['Id_Acteur'];

            if($req['Act_Statut'] == "eleveur"){
                header('Location: accueil_eleveur.php');
                exit;
            }elseif($req['Act_Statut'] == "abatteur"){
                header('Location: accueil_abatteur.php');
                exit;
            }elseif($req['Act_Statut'] == "transformateur"){
                header('Location: accueil_transformateur.php');
                exit;
            }elseif($req['Act_Statut'] == "transporteur"){
                header('Location: accueil_transporteur.php');
                exit;
            }elseif($req['Act_Statut'] == "distributeur"){
                header('Location: accueil_distributeur.php');
                exit;
            }elseif($req['Act_Statut'] == "consommateur"){
                header('Location: accueil_consommateur.php');
                exit;
            }
            
        }else{
            header('Location: index.php');
            exit;
        }
    }
}

?>