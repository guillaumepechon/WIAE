<?php 
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WAIE - Mes Déclarations</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
</head>
<body>
    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <a class="navbar-brand mb-0 h1 btn text-white" href="accueil_abatteur.php">What Am I Eating ?</a>
            <div>
                <a href="pre_decla_abatteur.php" class="btn mb-0 text-white" style="font-size: 17px;">Déclarer un abattage</a>
                <a href="mes_decla_abatteur.php" class="btn mb-0 text-white" style="font-weight: bold; font-size: 17px;">Voir mes déclarations</a>
            </div>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->
    <h1 class="text-center text-white mt-5">Vos déclarations</h1>

    <script>

        /*var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://localhost:3000/getMes_Declarations_Eleveur",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            "Postman-Token": "3dee017d-5b67-4a0d-aa9a-8cbe13eab144"
        },
        "data": {
            "Id_Eleveur": "1"
        }
        }

        $.ajax(settings).done(function (response) {
        console.log(response);
        var mode = response[0];
        var adresse = response[1];
        var id_bovin = response[2];
        var race = response[3];
        var antibio = response[4];
        var alimentation = response[5];
        });*/

        
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:3000/postList_Declaration_Abatteur",
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                "Postman-Token": "46d6b387-ebf0-4a9d-9910-f189e1388d3f"
            },
            "data": {
                "Id_Abatteur": "<?= $_SESSION['id_utilisateur'] ?>"
            }
        }

        $.ajax(settings).done(function (response) {
        
            var settings2 = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:3000/getList_Declaration_Abatteur",
            "method": "GET",
            "headers": {
                "cache-control": "no-cache",
                "Postman-Token": "b7e265fb-a8c9-43e4-a1b4-004f775acf49"
            }
            }

            $.ajax(settings2).done(function (response2) {
                console.log(response2);
            //var json = JSON.parse(response2)
            //var mode = response2[0][1];
            
            var body = document.getElementsByTagName("body")[0];
            var tbl = document.createElement("table");
            var tblBody = document.createElement("tbody");

            for( var i=0; i < response2.length+1; i++ ) {
                var row = document.createElement("tr");
                for ( var j=1; j < 8; j++ ) {
                    if(i==0) {
                        if(j==1){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("Déclarations");
                            cell.appendChild(cellText);
                            row.appendChild(cell); 
                        }else if(j==2){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("NNIB");
                            cell.appendChild(cellText);
                            row.appendChild(cell);  
                        }else if(j==3){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("Date de Réception");
                            cell.appendChild(cellText);
                            row.appendChild(cell); 
                        }else if(j==4){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("Date d'Abattage");
                            cell.appendChild(cellText);
                            row.appendChild(cell); 
                        }else if(j==5){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("Lieu d'Abattage");
                            cell.appendChild(cellText);
                            row.appendChild(cell); 
                        }else if(j==6){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("Type de saignée");
                            cell.appendChild(cellText);
                            row.appendChild(cell); 
                        }else if(j==7){
                            var cell = document.createElement("td");
                            var cellText = document.createTextNode("Date Limite de Consommation");
                            cell.appendChild(cellText);
                            row.appendChild(cell); 
                        }
                          
                    }else if(i!=0 && j==1) {
                        var cell = document.createElement("td");
                        var cellText = document.createTextNode("Déclaration "+i);
                        cell.appendChild(cellText);
                        row.appendChild(cell);
                    }else{
                        var cell = document.createElement("td");
                        var cellText = document.createTextNode(response2[i-1][j-1]);
                        cell.appendChild(cellText);
                        row.appendChild(cell);
                    }
                }
                tblBody.appendChild(row);
            }

            tbl.appendChild(tblBody);
            // appends <table> into <body>
            body.appendChild(tbl);
            // sets the border attribute of tbl to 2;
            tbl.setAttribute("border", "2");
            });

        });
        

    </script>
</body>
</html>