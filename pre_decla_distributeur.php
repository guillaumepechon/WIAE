<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WAIE - Déclaration Distributeur</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <a class="navbar-brand mb-0 h1 btn text-white" href="accueil_abatteur.php">What Am I Eating ?</a>
            <div>
                <a href="pre_decla_distributeur.php" class="btn mb-0 text-white">Déclarer une distribution</a>
                <a href="mes_decla_distributeur.php" class="btn mb-0 text-white" style="font-size: 17px;">Voir mes déclarations</a>
            </div>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->
    <h1 class="text-center text-white mt-5" style="margin-bottom: 70px;">Etape 1 : Recherche du produit</h1>
    
   
        <div class="col-4 card text-center p-3" style="border-radius: 20px; margin-right: auto; margin-left: auto;">
            <label for="itp" class="font-weight-bold mt-2">Identifiant de Traçabilité du Produit</label>
            <div class="form-inline mr-auto ml-auto mb-3">
                <input type="text" class="form-control" name="itp" id="itp" placeholder="Ex: FR7512345678001" minlength="15" maxlength="15">
                <button type="submit" class="ml-4 btn btn-success" onclick="search('itp', 'ok', 'ko', 'oko')">Rechercher</button>
            </div>
        </div>

        <div class="alert alert-success w-75 text-center p-4 mt-4" id="ok" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;" role="alert">
            <h3 class="mb-5">Le produit a été trouvé !</h3>
                <div class="w-75 m-auto">
                    <div class="row">
                        <div class="col">
                            <p>ITP : <strong id="ok_itp"></strong></p>
                        </div>
                        <div class="col">
                            <p>Mode de transport: <strong id="ok_md_trans"></strong></p>
                        </div>
                        <div class="col">
                            <p>Date d'arrivée': <strong id="ok_dt_ar"></strong></p>
                        </div>
                    </div>
                    <div class="text-center">
                        <p>Lieu de provenance : <strong id="ok_lieu"></strong></p>
                    </div>
                </div>
            <h5 class="mt-5">Vous pouvez maintenant déclarer une distribution</h5>
            <a id="lien_decla" href="" class="btn btn-info">Continuer</a>
        </div>

        <div class="alert alert-danger w-25 text-center p-4 mt-4" id="oko" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;" role="alert">
            <p>La distribution du produit a déjà été déclaré! Merci de vérifier l'ITP</p>
        </div>

        <div class="alert alert-danger w-25 text-center p-4 mt-4" id="ko" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;" role="alert">
            <p>Le produit n'a pas été trouvé! Merci de vérifier le ITP</p>
        </div>
    
<script>
    function search(itp_input, ok, ko, oko){
        var id = document.getElementById(itp_input).value;
        var divok = document.getElementById(ok);
        divok.style.display = "none";
        var divko = document.getElementById(ko);
        divko.style.display = "none";
        var divoko = document.getElementById(oko);
        divoko.style.display = "none";


        if(id.length < 15 ){
            var div = document.getElementById(ko);
            div.style.display = "";
        }else{
            
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost:3000/getDeclaration_Transporteur",
                "method": "POST",
                "headers": {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "cache-control": "no-cache"
                },
                "data": {
                    "ITP": id
                }
            }

            $.ajax(settings).done(function (response) {
                if(id == response[1]){

                    var settings2 = {
                        "async": true,
                        "crossDomain": true,
                        "url": "http://localhost:3000/getDeclaration_Distributeur",
                        "method": "POST",
                        "headers": {
                            "Content-Type": "application/x-www-form-urlencoded",
                            "cache-control": "no-cache"
                        },
                        "data": {
                            "ITP": id
                        }
                    }

                    $.ajax(settings2).done(function (response2) {
                        if(id != response2[1]){

                            var a = document.getElementById('lien_decla');
                            a.href = "decla_distributeur.php?itp=" + id;
                            var div = document.getElementById(ok);
                            div.style.display = "";
                            document.getElementById('ok_itp').innerHTML = response[1];
                            document.getElementById('ok_md_trans').innerHTML = response[2];
                            document.getElementById('ok_dt_ar').innerHTML = response[8];
                            document.getElementById('ok_lieu').innerHTML = response[5];

                        }else{
                            var div = document.getElementById(oko);
                            div.style.display = "";
                        }
                    });

                }else{
                    var div = document.getElementById(ko);
                    div.style.display = ""; 
                }
            });
        }
    }
        
</script>
</body>
</html>