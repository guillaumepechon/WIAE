<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>WAIE - Déclaration Transformateur</title>
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.7.2/css/all.min.css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"/>
    <link rel="stylesheet" href="./main.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
</head>
<body>
    <!-- début navbar -->
        <div class="d-flex flex-row align-items-center justify-content-between p-2">
            <a class="navbar-brand mb-0 h1 btn text-white" href="accueil_abatteur.php">What Am I Eating ?</a>
            <div>
                <a href="pre_decla_transformateur.php" class="btn mb-0 text-white" style="font-weight: bold; font-size: 17px;">Déclarer une transformation</a>
                <a href="mes_decla_transformateur.php" class="btn mb-0 text-white" style="font-size: 17px;">Voir mes déclarations</a>
            </div>
            <div>
                <a href="logout.php" class="btn btn-danger" style="color: white;">Déconnexion</a>
            </div>
        </div>
        <hr color="white" style="height: 1px; margin-top: -1px;">
    <!-- fin navbar -->
    <h1 class="text-center text-white mt-5" style="margin-bottom: 70px;">Etape 1 : Recherche du bovin</h1>
    
   
        <div class="col-4 card text-center p-3" style="border-radius: 20px; margin-right: auto; margin-left: auto;">
            <label for="id_bovin" class="font-weight-bold mt-2">Numéro National d'Identifiant des Bovins</label>
            <div class="form-inline mr-auto ml-auto mb-3">
                <input type="text" class="form-control" name="id_bovin" id="id_bovin" placeholder="Ex: FR7512345678">
                <button type="submit" class="ml-4 btn btn-success" onclick="search('id_bovin', 'ok', 'ko')">Rechercher</button>
            </div>
        </div>

        <div class="alert alert-success w-75 text-center p-4 mt-4" id="ok" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;" role="alert">
            <h3 class="mb-5">Le bovin a été trouvé !</h3>
                <div class="w-75 m-auto">
                    <div class="row">
                        <div class="col">
                            <p>NNIB : <strong id="ok_id_bovin"></strong></p>
                        </div>
                        <div class="col">
                            <p>Date d'Abattage: <strong id="ok_dt_abat"></strong></p>
                        </div>
                        <div class="col">
                            <p>Date Limite Conso : <strong id="ok_dt_lc"></strong></p>
                        </div>
                    </div>
                    <div class="text-center">
                        <p>Lieu d'abattage : <strong id="ok_lieu"></strong></p>
                    </div>
                </div>
            <h5 class="mt-5">Vous pouvez maintenant déclarer une transformation</h5>
            <a id="lien_decla" href="" class="btn btn-info">Continuer</a>
        </div>

        <div class="alert alert-danger w-25 text-center p-4 mt-4" id="ko" style="margin-left: auto; margin-right: auto; border-radius: 10px; display: none;" role="alert">
            <p>Le bovin n'a pas été trouvé! Merci de vérifier le NNIB</p>
        </div>
    
<script>
    function search(id_bovin, ok, ko){
        var id = document.getElementById(id_bovin).value; 
        var divok = document.getElementById(ok);
        divok.style.display = "none";
        var divko = document.getElementById(ko);
        divko.style.display = "none";

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:3000/getDeclaration_AbatteurById_Bovin",
            "method": "POST",
            "headers": {
                "Content-Type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache"
            },
            "data": {
                "Id_Bovin": id
            }
        }

        $.ajax(settings).done(function (response) {
            if(id == response[1]){
                var a = document.getElementById('lien_decla');
                a.href = "decla_transformateur.php?id_bovin=" + id;
                var div = document.getElementById(ok);
                div.style.display = "";
                document.getElementById('ok_id_bovin').innerHTML = response[1];
                document.getElementById('ok_dt_abat').innerHTML = response[3];
                document.getElementById('ok_dt_lc').innerHTML = response[6];
                document.getElementById('ok_lieu').innerHTML = response[4];
                
            }else{
                var div = document.getElementById(ko);
                div.style.display = ""; 
            }
        });
    }
        
</script>
</body>
</html>