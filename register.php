<?php 
session_start();
include_once('includes.php');
if(!empty($_POST)){
    extract($_POST);
    $valid = true;
    if(isset($_POST['inscription']))
    {
        $prenom_r = trim($prenom_r);
        $nom_r = trim($nom_r);
        $email_r = htmlentities(strtolower(trim($email_r)));
        $mdp_r = trim($mdp_r);
        $conf_mdp_r = trim($conf_mdp_r);

        if(empty($prenom_r)){ // Vérification qu'il y est bien un prenom de renseigné
            $valid = false;
            $_SESSION['err_prenom_r_vide'] = "Il faut rentrer votre prénom pour s'inscrire";
            $_SESSION['tmp_err_prenom_r_vide'] = 1;
        }

        if(empty($nom_r)){ // Vérification qu'il y est bien un nom de renseigné
            $valid = false;
            $_SESSION['err_nom_r_vide'] = "Il faut rentrer votre nom pour s'inscrire";
            $_SESSION['tmp_err_nom_r_vide'] = 1;
        }

        if(empty($email_r)){ // Vérification qu'il y est bien un mail de renseigné
            $valid = false;
            $_SESSION['err_email_r_vide'] = "Il faut rentrer votre email pour s'inscrire";
            $_SESSION['tmp_err_email_r_vide'] = 1;
        }

        if(empty($mdp_r)){ // Vérification qu'il y est bien un mot de passe de renseigné
            $valid = false;
            $_SESSION['err_mdp_r_vide'] = "Il faut rentrer votre mot de passe pour s'inscrire";
            $_SESSION['tmp_err_mdp_r_vide'] = 1;
        }

        if(empty($conf_mdp_r)){ // Vérification qu'il y est bien une confirmation de mot de passe de renseigné
            $valid = false;
            $_SESSION['err_conf_mdp_r_vide'] = "Il faut confirmer votre mot de passe pour s'inscrire";
            $_SESSION['tmp_err_conf_mdp_r_vide'] = 1;
        }

        if(empty($statut_r)){ // Vérification qu'il y est bien un statut de selectionné
            $valid = false;
            $_SESSION['err_statut_r_vide'] = "Il faut sélectionner votre statut pour s'inscrire";
            $_SESSION['tmp_err_statut_r_vide'] = 1;
        }

        if($mdp_r != $conf_mdp_r){
            $valid = false;
            $_SESSION['err_mdp_r'] = "Les deux mots de passe ne correspondent pas";
            $_SESSION['tmp_err_mdp_r'] = 1;
        }

        $req = $DB->query("SELECT * FROM acteur WHERE Act_Email = ?", array($email_r));
        $req = $req->fetch();

        if ($req != null){
            $valid = false;
            $_SESSION['err_email_r_false'] = "L'email est déjà utilisé par un utilisateur!";
            $_SESSION['tmp_err_email_r_false'] = 1;
        }

        if($valid) {
            $ins = $DB->insert("INSERT INTO acteur (Act_Prenom, Act_Nom, Act_Email, Act_Mdp, Act_Statut) VALUES (?, ?, ?, ?, ?) ", array($prenom_r, $nom_r, $email_r, $mdp_r, $statut_r));
            $_SESSION['register_ok'] = "Vous vous êtes bien inscrit !";
            $_SESSION['tmp_register_ok'] = 1;
            header('Location: index.php');
            exit;
        }else{
            header('Location: index.php');
            exit;
        }

    }
}
?>